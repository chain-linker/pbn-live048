---
layout: post
title: "투어 39년 만의 신인 3관왕 “메이저 퀸 박성현(Park Sunghyun) 골프선수”"
toc: true
---



  대한민국의 제차 골프 선수이자 중제 LPGA 투어 프로인 그녀는 LPGA 투어 39년 만의 신인 3관왕 달성자(올해의 선수상 - 신인상 - 여태 1위)입니다. 더욱이 그녀는 KLPGA 한도 시즌 최다 아직껏 달성자(총액 1,333,090,667원으로, 이조 외에 초대 받은 이국 대회에서의 호성적으로 2016년 임계 해에만 21억 종작 벌어 들였음)이기도 합니다.

  그녀는 '신데렐라'입니다. 피차일반 자세히 나가는 스타급 선수들이 엘리트코스를 밟아온 것과 달리 그녀는 시련과 사고, 불운을 딛고 오랜 무명 생활을 거쳐 평생 첫 우승을 으뜸 권위있는 내셔널타이틀 대회(한국여자오픈)에서 따내며 드라마 같은 인생의 반전을 이루어냈습니다.



  그녀는 171cm, 60kg. 작은 얼굴에 긴 다리. 언뜻보기에 '모델'이 보다 어울릴 것 같은 몸매에서 280야드의 무시무시한 장타가 나옵니다. 비결을 물어보자 그녀는 "제가 오죽이나 통뼈에요. 몸이 유연하지는 않은데 히팅(공을 때리는)하는 능력이 조금 좋은 편이에요. 태권도 공인 3단인 엄마를 닮아서 힘을 쓸 줄 아는 것 같아요. 장부 큰 타이틀 장타의 비결은 골반인 것 같아요. 내려올 때 골반 턴이 충족히 다른 선수보다 더더욱 매우 돼서 임팩트 순간에 그런 폭발적인 힘이 나오는 것 같아요. 실지 거리가 참으로 난다는 건 비장의 무기가 될 이운 있는 것 같아요. 누구나 잔뜩 나가는 건 아니잖아요. 제가 많이 나가는 건 진짜로 축복 받은 일이죠."라고 말했습니다. 엄청난 실력과 장타력, 슈퍼스타의 자질을 보유한 보이쉬한 매력을 가진 그녀의 이름은 박성현 골프선수입니다.

 ◆ 박성현 업 골퍼의 프로필

 ☞ 출 생 : 1993년 9월 21일, 대한민국
 ☞ 옥황상제 체 : 172cm, B형

 ☞ 학 력 : 유현초-현일중-현일고-한국외대 글로벌캠퍼스 국제스포츠레저학부

 ☞ 핏줄 육성 : 골프

 ☞ 데 뷔 : 2012년 10월 KLPGA 입회

 ☞ 소 정 : 넵스(2014~2016) 하나금융그룹(2017~)

 ☞ 스타 함자 : 남달라'.' 학사 시절, 선생님이 "남과 다르지 않으면 성공할 삶 없다



 ◆ 박성현 프로 골퍼의 평균 샷거리

 ☞ 드라이버 : 250m
 ☞ 3번 우드 : 210m~220m

 ☞ 4번 아이언 : 180m~185m

 ☞ 5번 아이언 : 170m

 ☞ 6번 아이언 : 160m

 ☞ 7번 아이언 : 150m

 ☞ 8번 아이언 : 140m

 ☞ 9번 아이언 : 130m

 ☞ 피  칭 : 120m

 ☞ 19도 유틸리티 : 195m~205m

 ☞ 50도 웨지 : 90m~110m

 ☞ 54도 웨지 : 80m~90m

 ☞ 58도 웨지 : 80m 이하, 그린사이드 벙커



  박성현 선수는 가녀린 몸매와 보이쉬한 외모, 더욱이 목소리까지 초등학교시절부터 변함이 없습니다. 엄청난 파워풀한 스윙 등으로 무장한, 여성 골프팬들 최고의 아이돌로 2015년부터 KLPGA의 스타로 떠올랐습니다. 그녀는 2015 시즌의 지배자였던 메이저 퀸 썩 나은 경기력을 보여주며 전인지의 LPGA 투어 진출로 인해, 2016년 KLPGA의 새로운 여왕 자리에 오를 양반 강력한 후보로 떠올랐고 그리고 댁 기대를 뛰어 넘어 완벽하게 16년 시즌을 지배하며 KLPGA의 여왕이 되었습니다. 

  박성현 선수는 초청받은 LPGA 대회에서도 맹활약한 끝에 '우승 궁핍히 아직 순위만으로 LPGA 직행한 최초의 사례'를 기록하였습니다. 17년 시즌에 초반에 다소 어려움을 겪었지만 US Women`s Open에서 우승한 만미 꾸준하게 좋은 성적을 내었고, 끝판 39년 만에 신인으로서 '올해의 선수상'과 '상금 1위'까지 동시에 받는 최고의 시즌을 보냈습니다.



 ▶ 역대 KLPGA 한국여자오픈 우승자(KLPGA 5대 메이저 한가운데 하나)

 ☞ 2014년 제28회 김효주→2015년 제29회 박성현→2016년 제30회 안시현

 ▶ 역대 LPGA U.S.여자오픈 우승자(LPGA 5대 메이저 새중간 하나)

 ☞ 2016년 제71회 브리타니 랭→2017년 제72회 박성현→2018년 제73회



 ◆ 박성현 프로 골퍼-불운을 연습으로 이겨내다

  박성현 선수는 초등학교 2학년 잠시 골프를 시작했습니다. 그녀는 2007년 중학교 2학년 기간 골프를 계속하기 위해 한가운데 대청중학교에서 서구 현일중학교로 전학을 갔고, 여기서 두각을 나타내 전모 국가대표 상비군에 뽑혔습니다. 2010년 현일고 2학년 수유 국가대표가 됐는데 갑작스럽게 드라이버 샷 입스(yips)가 찾아와 광저우 아시안게임 대표자 선발전에서 고배를 들었습니다. 이전 동안 시작된 드라이버샷의 입스가 이듬해(2011년) 차례표 데뷔 후에도 계속됐고, 2012년까지 무려 3년 시가 그녀를 짓눌렀습니다.

  그녀는 "힘들었던 자기 시간을 되돌이켜 보면 더없이 답답하고 진짜로 미치겠더라고요. 티샷 어드레스를 하면 안맞을 것 같은 생각부터 들었어요. 제일 큰 거는 멘탈인 것 같아요. 마음에서 오는 게 스케일 그렇게 스윙이 멀쩡하더라도 임팩트 순간에 손을 돌린다거나 도통 공을 못 치고 떠민다거나 말도 중 대단히 오른쪽으로 가는 샷도 나오고, 오른쪽으로 안가기 위해서 손을 써서 왼쪽으로 훅 감기는 샷이 나오거나 그런게 대부분이었죠. 미팅 허리 제한 홀에 O.B(out of bounds)가 서너개씩 나고, 무리 5홀에서 12타, 13타 치고 그런 [골프입스](https://screwslippery.com/sports/post-00029.html) 일들이 많았어요. 그래도 포기하지 않았어요. 손바닥이 까지고 부르트도록 연습하고 도로 연습했어요. 자신도 모르게 서서히 샷이 곧바로 잡혀갔어요“라고 옛일을 회상했습니다.
 

 그녀는 엎친 데 덮친 격으로  맹장수술과 교통사고까지 이어져 3개월 병원에 입원하면서 정규투어 입성이 재개 늦어졌습니다. 박성현 선수는 부상 회복 뒤 다행히도 쉬는 감 감각이 떨어지진 않았서 출전한 경기에서 좋은 모습을 보여 주었습니다. 드림투어 1, 2차 준우승에 곧장 3차 우승을 하였고, 점프투어에서도 3승을 거두며 실력자로 인정받았습니다. 2013년 2부 투어 상금왕에 오르면서 선년 1부 투어 시드를 따냈고, 1부 투어 피복 2년 차(2015 시즌)에 기어코 '메이저 퀸'에 등극하며 자기 동안의 마음고생을 훌훌 털어버렸습니다.

  
 ◆ 박성현 관직 골퍼의 시즌별 성적

 ☞ 2015년 시즌

  그녀는 절치부심하고 맞이한 2015년 초반도 전년과 유별 다르지 않게 부진했습니다. 후배들인 전인지, 고진영 등이 2승씩을 거둔 상황에서 본인은 컷탈락과 하위권을 전전했습니다. 그녀는 계속된 1위 찬스에서 번번히 우승을 차지하지 못하며 힘든 시즌을 보냈습니다. 그런 그녀에게 기회가 찾아왔습니다. 인천 베어즈베스트청라골프클럽에서 열린 시즌 첫 메이저대회인 기아자동차 제29회 한국여자오픈(총상금 7억원·우승상금 2억원) 종극 라운드에서 이정민 선수를 누르고 우승을 차지하며 그토록 꿈꿔 왔던 업 첫승을 메이저에서 거두며 골프계의 신데렐라로 떠올랐습니다.

  첫 우승 이래 상위권에 바지런히 등장하며 계추 우승에의 기대감을 높인 박성현은, 9월에 열린 KDB 대우증권 CLASSIC에서 전년도 우승자인 전인지를 누르고 2승째를 거두며 차근차근 성장하였습니다. 종단 그녀는 시즌 3승, 7억원대의 상금으로 여태껏 2위-대상 5위-평균타수 8위-다승 2위 등 전년도와는 비교도 할 생명 없는 훌륭한 성적으로 프로 2년차 시즌을 마쳤습니다.

 ☞ 2016년 시즌

  그녀는 KLPGA 역사상 밖주인 압도적인 시즌을 보냈습니다. 역중 대회에서의 첫 승을 달성한지 모처럼 일주일만에 KLPGA 투어 넥센 세인트나인 마스터즈 2016에서도 우승을 차지했습니다. 본인에게는 통산 여섯 번째 우승이자, 올해 시즌 세 번째 우승입니다. 이로써 이해 시즌 KLPGA에서 본인이 참가한 대회들에서는 전승(3연승)을 거두며, 2016시즌 승률 100%라는 박성현 선수의 경이로운 기록은 당분간 현재진행형으로 남게 되었습니다. 2016년도 KLPGA 시상식의 주인공이 되었습니다.

  대상은 고진영에게 넘겨 주었으나 네년 외의 주요 부분을 송두리째 시상하였고 고진영의 경기력이 나무랄데 없었기에 목표물 수상에도 이견은 없었으나 반면 불구하고 전체 시상식의 주인공은 박성현이었습니다. “2016년은 쉼 가난히 달려온 제한 해였다. 좋은 상을 받을 복수 있어 데이터 영광이다. 앞으로도 목표를 향해 한발 한발 나아가겠다”고 소감을 밝혔습니다.



 ☞ 2017년 시즌

 All Time급 수퍼 루키 시즌이자 역대 애초 LPGA Rookie Year World No.1 Player에 당당히 이름을 새겼습니다. 그녀는 마침내 LPGA에 데뷔했습니다. 데뷔하고 나서 'U.S. 여자오픈'에서 우승하며 LPGA 첫 승을 메이저 회합 우승으로 장식했습니다. 계속해서 좋은 모습을 보인 그녀는 결초 LPGA 투어 역대 최초의 데뷔 첫 손해 세상 1위라는 쾌거를 달성하였습니다. "LPGA 데뷔할 시기 인간 10위였는데, 1위라니 꿈만 같다. 그럼에도 천하만국 1위가 됐다고 해서 끝이 아니기 그렇게 더욱 노력하는 자세가 필요할 것 같다."며 앞으로도 꾸준한 활약으로 탑 클래스 선수 다운 모습을 보여 주겠다는 각오를 밝혔습니다.

  그러나 어렵게 도달한 세상인심 1위는 '일단' 1주 만에 내려 오게 되었다. 도리어 '올해의 선수상'을 유소연과 공동으로 수상. 39년 만의 신인 3관왕이라는 대기록을 달성하며 시즌을 마무리했습니다. 이런즉 맹활약 덕인지 미국 스포츠아카데미(USSA)가 매년 선정하는 '올해의 요조숙녀 선수'로 박성현 선수를 선정하였습니다. 한국 선수로는 2010년, 2013년의 김연아 후 최초입니다.

 ☞ 2018년 시즌

  18년 3월 언급 현재, 280야드로 드라이버 비거리 1위, 81%로 그린적중률 공유 3위 등에 올라 있지만, 페어웨이 안착률과 퍼팅에서 심각한 부진을 겪으면서 관망 외의 부침을 보이며, 'Kia Classic'에서는 아이언까지 부진한 모습을 겪으며 오랫 만에 컷 탈락. 절치부심 하고 맞이한 시즌 첫 메이저 회취 'ANA 인스퍼레이션'에서는 시즌 첫 TOP 10 입성에 성공했습니다.


 ◆ 박성현 생업 골퍼의 우승 경력

 ▶ 18년 귀재 KLPGA 통산 10승, LPGA 2승 등 전 고해 투어 통산 12승 필기 중

 ☞ 2015 KLPGA 투어 기아자동차 제29회 한국여자오픈 골프선수권대회

 ☞ 2015 KLPGA 투어 KDB 대우증권 CLASSIC 2015

 ☞ 2015 KLPGA 투어 OK저축은행 박세리 INVITATIONAL

 ☞ 2016 KLPGA 투어 현대차 중국여자오픈

 ☞ 2016 KLPGA 투어 삼천리 Together Open 2016

 ☞ 2016 KLPGA 투어 넥센 · 세인트나인 마스터즈 2016

 ☞ 2016 KLPGA 투어 두산 매치플레이 챔피언십 2016

 ☞ 2016 KLPGA 투어 제주 삼다수 마스터스 2016

 ☞ 2016 KLPGA 투어 BOGNER MBN 여자오픈 2016

 ☞ 2016 KLPGA 투어 한화금융 클래식 2016

 ☞ 2017 LPGA 투어 U.S. Women`s Open

 ☞ 2017 LPGA 투어 캐나다 퍼시픽 여자오픈



 ◆ 박성현 소업 골퍼의 상금 및 순위

 ▶ KLPGA

 ☞ 2014년 : 120,586,237원(34위)

 ☞ 2015년 : 736,690,082원(2위)

 ☞ 2016년 : 1,333,090,667원(1위)

 ▶ LPGA

 ☞ 2017년 : $2,335,883(1위)

 ☞ 2018년 : $91,923(43위, 현재)


 ◆ 박성현 상직 골퍼의 별별 이야기

 ☞ 팬클럽명은 '남달라'. 골프백에 새겨진 ‘남달라’라는 문구로 인해 정해진 클럽명임.(좋은 선수가 되기 위해선 남달라야 한다고 생각했고 마침내 그런 글자를 새겨 넣었다”고 말한 바 있음.)

 ☞ 조용한 성격이라 투어에서 친한 선수는 많지 않은 뼈 김지희 프로와 절친임.

 ☞ 2015 KLPGA 시상식에서 오프숄더 드레스라는 반전 매력을 보여주었음.

 ☞ 2015 KLPGA 시상식 인기상에서 전년도 수상자인 전인지를 2위로 내려 앉히고 새로운 수상자가 되었음.(16년도에서 수상하며 2연패를 달성)

 ☞ 16년 땅덩이 천지 기수 어려운 이웃들을 돕기 위해 1억원의 상금을 기부하였음.(15년 연말에도 1억원을 기부
 ☞ '하나금융그룹'과 2년간 메인 스폰서 계약을 맺었음. 그편 가항 에이전시, 캐디, 용품 등의 계약을 거듭 맺음.

 ☞ 2017 시즌 LPGA 데뷔 최후 첫 준우승을 거둔 볼빅 챔피언십 이후, 캐디를 전인지의 전 캐디였던 데이비드 존스로 교체했음.(공교롭게 비슷한 시기에 전인지도 박성현의 전 캐디였던 콜린 칸으로 교체하여 반드시 캐디 트레이드를 애한 모양새가 되었음.)

 ◆ 박성현 서차 골퍼의 바램
  박성현 선수는 '볼매'입니다. 그녀는 볼수록 매력이 넘치다 못해 넘쳐 흐릅니다. 그녀는 아울러 반전 매력을 가지고 있습니다. 호리 호리한 몸에서 뿌려대는 280야드의 폭발적인 장타력, 모자를 벗었을 나간 생각보다 귀엽고 여성스런 얼굴, 그리하여 가녀린 얼굴에서 나오는 중저음의 소신 등 모든 것이 반전 매력입니다.

  가정 형편이 어려워 마음대로 된 레슨도 받지 못했다는 그녀는 “중학교 3학년 도리 함께 운동했던 1년 후배 남학생의 아버지가 박성희 프로님이었는데 제가 치는 걸 보시더니 돈도 안받고 레슨을 해주셨어요.”라고 말했습니다. 그녀의 목표는 "정말 롱런하는 선수가 되고 싶어요. 개인적으로는 40살까지 투어생활을 할 생각입니다."라고 말했습니다. 그녀의 바램대로 부상없이 롱런하는 선수가 되기를 바라며 바지런스레 응원하겠습니다.

